package com.opesystems.android.message.handler;

import android.os.Parcel;
import android.os.Parcelable;

public class Message implements Parcelable {
	
	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Message(){}
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	public static final Parcelable.Creator<Message> CREATOR = new Parcelable.Creator<Message>() {
		public Message createFromParcel(Parcel in) {
			return new Message(in);
		}

		public Message[] newArray(int size) {
			return new Message[size];
		}
	};

	@Override
	public void writeToParcel(Parcel parcel, int data) {
		// TODO Auto-generated method stub
		parcel.writeInt(statusCode);
		parcel.writeString(message);
	}
	
	private Message(Parcel in){
		statusCode = in.readInt();
		message = in.readString();
	}

	private int statusCode;
	private String message;

}
