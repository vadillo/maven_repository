package com.opesystems.android.message.handler;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

public class MessageHandler {
	
	public static final String TAG = MessageHandler.class.getSimpleName();
	
	public static MessageHandler getInstance(Context context){
		if(INSTANCE == null){
			INSTANCE = new MessageHandler(context.getApplicationContext());
		}
		return INSTANCE;
	}
	
	public static Message decodeMessage(Intent intent){
		Message msg = intent.getParcelableExtra(MESSAGE_DATA);
		return msg;
	}
	
	public void sendMessage(String message, int statusCode){
		Intent intent = createIntentMessage(message, statusCode);
		Log.i(TAG, "Broadcasting Message" + statusCode);
		mBroadcastManager.sendBroadcast(intent);		
	}
	
	public void sendMessage(String message){
		sendMessage(message, 0);
	}
	
	public void unregisterCurrentReceiver(){
		if(mCurrentReceiver != null){
			Log.i(TAG, "UnregisteredReceiver");
			unregisterReceiver(mCurrentReceiver);
		}
	}
	
	public void unregisterReceiver(BroadcastReceiver receiver){
		mBroadcastManager.unregisterReceiver(receiver);
		mCurrentReceiver = null;
	}
	
	public void registerReceiver(BroadcastReceiver receiver){
		Log.i(TAG, "RegisteringReceiver");
		unregisterCurrentReceiver();
		IntentFilter filter = createIntentFilter();
		mBroadcastManager.registerReceiver(receiver, filter);
		mCurrentReceiver = receiver;
	}
	
	private Intent createIntentMessage(String message, int statusCode){
		Intent intent = new Intent(INTENT_ACTION);
		Message msg = new Message();
		msg.setMessage(message);
		msg.setStatusCode(statusCode);
		intent.putExtra(MESSAGE_DATA, msg);
		return intent;
	}
	
	private IntentFilter createIntentFilter(){
		IntentFilter filter = new IntentFilter(INTENT_ACTION);
		return filter;
	}
	
	private MessageHandler(Context context){
		mContext = context;
		mBroadcastManager = LocalBroadcastManager.getInstance(mContext);
	}
	
	private static final String MESSAGE_DATA = "Data";
	private static final String INTENT_ACTION = "LocalBroadcastIntent";
	
	private static MessageHandler INSTANCE;
	
	private BroadcastReceiver mCurrentReceiver;
	private LocalBroadcastManager mBroadcastManager;
	private Context mContext;
}
